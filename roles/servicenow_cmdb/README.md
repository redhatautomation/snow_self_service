# redhatautomation.snow_self_service.servicenow_cmdb
This role adds a configuration item to the Now CMDB for use in the ServiceNow Self-Service demonstration.

## Requirements
* this role requires Python 2.7 or greater

## References
* https://galaxy.ansible.com/servicenow/itsm

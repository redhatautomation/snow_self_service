# SNOW Self-Service Ansible Roles

This repo hosts the roles for the `redhatautomation.snow_self_service` Ansible Collection. 

The collection includes the following roles:
  * `servicenow_cmdb` 
  * `slack`
  * `snow_ticket`
  * `vmware`

For more information see the individual role's documentation.

## Python & Libraries
#### Python Version Compatibility
  * this collection is compatible with Python 3.6+

#### Python libraries
The following python libraries are required to use this collection:
  * PyVmomi (`pyvmomi`)
  * PySnow (`pysnow`)
  * Requests (`requests`)

## Installation
Before using the this collection, you need to install it with the Ansible Galaxy CLI:

```console
ansible-galaxy collection install redhatautomation.snow_self_service
```

You can also include it in a `requirements.yml` file and install it via `ansible-galaxy collection install -r requirements.yml`, using the format:

```yaml
---
collections:
  - name: redhatautomation.snow_self_service
    version: 1.0.3
```

#### Python Dependencies
In order to use this collection, the host which runs it (usually Tower) will need to have the required python libraries installed. The following command will install them:
```console
pip install pyvmomi pysnow requests
```

## Setup
A variety of ServiceNow setup steps are required for this demonstration to function. See [Ansible + ServiceNow Part 3: Making outbound RESTful API calls to Red Hat Ansible Tower](https://www.ansible.com/blog/ansible-servicenow-howto-part-3-making-outbound-restful-api-calls-to-ansible-tower) for helpful information on setting up ServiceNow and Ansible Tower for integration.

#### Now Script
The following workflow activity "Run Script" can be used to pass data from the Service Catalog request to Ansible Tower:
```javascript
try {
  var r = new sn_ws.RESTMessageV2('Lab Tower (Cheeseburgia)', 'Provision VM Workflow');
  r.setStringParameterNoEscape('RITM', current.number);
  r.setStringParameterNoEscape('VMWARE_NAME', current.variables.vm_name);
  r.setStringParameterNoEscape('VMWARE_OS_TEMPLATE', current.variables.vm_os_template);
  r.setStringParameterNoEscape('VM_SIZE', current.variables.vm_size);
  //override authentication profile
  //authentication type ='basic'/ 'oauth2'
  //r.setAuthenticationProfile(authentication type, profile name);

   //set a MID server name if one wants to run the message on MID
   //r.setMIDServer('MY_MID_SERVER');

   //if the message is configured to communicate through ECC queue, either
   //by setting a MID server or calling executeAsync, one needs to set skip_sensor
   //to true. Otherwise, one may get an intermittent error that the response body is null
   //r.setEccParameter('skip_sensor', true);

  var response = r.execute();
  var responseBody = response.getBody();
  var httpStatus = response.getStatusCode();
}
catch(ex) {
  var message = ex.message;
}
```

## Usage
Each of the roles in this collection can be used individually, however, they were built for a specific use case.

#### Playbooks
Each of the roles are intended to be used in together in a [Red Hat Ansible Automation Platform Controller](https://www.ansible.com/products/automation-platform) workflow. The playbooks of each template in the workflow would resemble the following:

```yaml
---
- name: Instanciate VM node and export variables for subsequent jobs
  hosts: localhost
  vars:
    # define role vars...
  roles:
    - redhatautomation.snow_self_service.vmware
```

```yaml
---
- name: Register new VM instance with SNOW CMDB
  hosts: localhost
  roles:
    - redhatautomation.snow_self_service.servicenow_cmdb
```

```yaml
---
- name: Update ServiceNow request
  hosts: localhost
  roles:
    - redhatautomation.snow_self_service.snow_ticket
```

```yaml
---
- name: Send slack message with updated VMWARE and SNOW data
  hosts: localhost
  roles:
    - redhatautomation.snow_self_service.slack
```

#### Variables
ServiceNow provides the following variables to the workflow:

```yaml
# ServiceNow request identifier
request_num: RITMxxxxx

# VM identifier to be used to provide hardware resources to VMWARE
vm_size: lc1.small

# the name to be given to the provisioned VM
vmware_name: test123

# a template to build the VM from
vmware_os_template: t-rhel8server-01.06.2021
```

## NAPS DA ServiceNow Self-Service Demo
These roles are used to build the [Self-Service Automation demo](https://napsda.github.io/ansible/links/snow-self-service.html).


## References
* [Ansible + ServiceNow Part 3: Making outbound RESTful API calls to Red Hat Ansible Tower](https://www.ansible.com/blog/ansible-servicenow-howto-part-3-making-outbound-restful-api-calls-to-ansible-tower)
* [Ansible for ServiceNow](https://www.youtube.com/watch?v=T8CtycTluos)
* [Ansible certified Content Collection for ServiceNow ](https://www.ansible.com/resources/webinars-training/red-hat-ansible-certified-content-collection-for-servicenow-ondemand)
* [ServiceNowDeveloper Instance](https://developer.servicenow.com/dev.do)

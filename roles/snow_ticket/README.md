# redhatautomation.snow_self_service.snow_ticket

This role updates a ServiceNow request item (record) in the Now CMDB for use in the ServiceNow Self-Service demonstration.

## Requirements 
The below requirements are needed on the host that executes this role.
* python pysnow (pysnow)
* python requests (requests)

## Refernces
* https://docs.ansible.com/ansible/latest/collections/servicenow/servicenow/snow_record_module.html
